<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Page</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
        include_once("includes/header.php");
        include_once("includes/navigation.php");
        include_once("includes/main.php");
        include_once("includes/footer.php");
    ?>
</body>
</html>