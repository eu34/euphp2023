<?php
    session_start();
    $server = "localhost";
    $user = "root";
    $password =  "";
    $db = "blog2023_2";

    $con = mysqli_connect($server, $user, $password, $db);
    mysqli_set_charset($con, "utf8mb4"); 

    if(isset($_POST['logout'])){
        session_destroy();
    }
    
    if(isset($_POST['auth'])){
        $email = $_POST['email'];
        $password  = $_POST['password'];
        // echo $email;
        // echo $password;
        $check_user_query = "SELECT * FROM users WHERE email='$email' AND password='$password'";
        $result_user = mysqli_query($con, $check_user_query);
        // print_r(mysqli_num_rows($result_user));
        if(mysqli_num_rows($result_user)){
            $_SESSION['user'] = $email;
        }
    }

    $name = "";
    $lastname = "";
    $email = "";
    
    if(isset($_POST['add'])){
        $name = $_POST['name'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email'];
        // echo $name;
        // echo $lastname;
        // echo $email; 
        $insert_query = "INSERT INTO users(name, lastname, email) VALUES ('$name', '$lastname', '$email')";
        mysqli_query($con, $insert_query);
    }

    if(isset($_POST['edit'])){
        $name = $_POST['name'];
        $lastname = $_POST['lastname'];
        $email = $_POST['email']; 
        $user_id = $_POST['user_id']; 
        // echo $name;
        // echo $lastname;
        // echo $email;
        $update_query = "UPDATE users SET name='$name', lastname='$lastname', email='$email'
                         WHERE id='$user_id'";
        mysqli_query($con,  $update_query);
    }

    if(isset($_GET['drop_user'])){
        $drop_user_id  = $_GET['drop_user'];
        // echo $drop_user_id;
        $delete_query = "DELETE FROM users WHERE id='$drop_user_id'";
        mysqli_query($con, $delete_query);
    }

    if(isset($_GET['edit_user'])){
        $edit_user_id  = $_GET['edit_user'];
        // echo $drop_user_id;
        $user_edit_query = "SELECT * FROM users WHERE id='$edit_user_id'";
        $edit_result = mysqli_query($con, $user_edit_query);
        $row = mysqli_fetch_assoc($edit_result);
        // print_r($row);
        $name =$row['name'];
        $lastname = $row['lastname'];
        $email = $row['email'];
    }

    $select_name_lastname = "SELECT id, name, lastname FROM users";
    $result_name_lastname = mysqli_query($con, $select_name_lastname);
    // print_r($result_name_lastname);
    $fullnames = mysqli_fetch_all($result_name_lastname);
    // echo "<pre>";
    // print_r($fullnames);
    // echo "</pre>";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <header>
        <?php
            if(!isset($_SESSION['user'])){
        ?>
        <form method="post">
            <input type="text" name="email" placeholder="email">
            <br><br>
            <input type="text" name="password" placeholder="password">
            <br><br>
            <button name="auth">ავტორიზაცია</button>
        </form>
        <?php
            }else{
        ?>
        <form method="post">
            <button name="logout">გასვლა</button>
        </form>
        <?php
            }
        ?>
    </header>
    <?php
        if(isset($_SESSION['user'])){
            include "insert.php";
        }
    ?>
    <div class="main">
        <div class="left">
            <?php
                foreach($fullnames  as $items){
                    echo "<a href='?user=$items[0]'>$items[1] $items[2]</a>";
                    if(isset($_SESSION['user'])){
                        echo " <a href='?drop_user=$items[0]'>DROP</a> <a href='?edit_user=$items[0]'>Edit</a>" ;  
                    } 
                    echo "<hr>";
                }
            ?>
        </div>
        <div class="right">
            <?php
                if(isset($_GET['user'])){
                    // echo $_GET['user'];
                    $user_id = $_GET['user'];
                    $select_full_info = "SELECT * FROM users WHERE id='$user_id'";
                    $result_full_info = mysqli_query($con,  $select_full_info);
                    $full_info = mysqli_fetch_assoc($result_full_info);
                    // print_r($full_info);
                    echo $full_info['name'];
                    echo "<br><br>";
                    echo $full_info['lastname'];
                    echo "<br><br>";
                    echo $full_info['email'];
                    echo "<br><br>";
                }
            ?>
        </div>
    </div>
    
</body>
</html>