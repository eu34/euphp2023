<?php
    include_once "blocks/header.php";
?>
<main>
    <?php
        include_once "blocks/nav.php";
        include_once "blocks/content.php";
    ?>
</main>
<?php
    include_once "blocks/footer.php";
?>