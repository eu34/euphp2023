<?php
    $server = "localhost";
    $user = "root";
    $password =  "";
    $db = "blog2023_2";

    $con = mysqli_connect($server, $user, $password, $db);
    mysqli_set_charset($con, "utf8mb4"); 

    $select_name_lastname = "SELECT id, name, lastname FROM users";
    $result_name_lastname = mysqli_query($con, $select_name_lastname);
    // print_r($result_name_lastname);
    $fullnames = mysqli_fetch_all($result_name_lastname);
    // echo "<pre>";
    // print_r($fullnames);
    // echo "</pre>";

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="main">
        <div class="left">
            <?php
                foreach($fullnames  as $items){
                    echo "<a href='?user=$items[0]'>$items[1] $items[2]</a>" ;
                    echo "<hr>";
                }
            ?>
        </div>
        <div class="right">
            <?php
                if(isset($_GET['user'])){
                    // echo $_GET['user'];
                    $user_id = $_GET['user'];
                    $select_full_info = "SELECT * FROM users WHERE id='$user_id'";
                    $result_full_info = mysqli_query($con,  $select_full_info);
                    $full_info = mysqli_fetch_assoc($result_full_info);
                    // print_r($full_info);
                    echo $full_info['name'];
                    echo "<br><br>";
                    echo $full_info['lastname'];
                    echo "<br><br>";
                    echo $full_info['email'];
                    echo "<br><br>";
                }
            ?>
        </div>
    </div>
</body>
</html>