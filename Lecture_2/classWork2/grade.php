<?php
    include "questions.php";
    // echo "<pre>";
    // print_r($_POST);
    // echo "</pre>";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Grade</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="home">
        <form action="result.php" method="post">
            <h1>PHP Quiz - Grade</h1>
            <div class="student-info">
                <h2>
                <?php
                    echo $_POST['st_name']." ".$_POST['st_lastname'];
                ?>
                <input type="hidden" placeholder="Student Name" name="st_name" value="<?=$_POST['st_name']?>">
                <input type="hidden" placeholder="Student Lastname" name="st_lastname" value="<?=$_POST['st_lastname']?>">
               </h2>
            </div>
            <table>
                <tr>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>Max Point</th>
                    <th>Grade</th>
                </tr>
                <?php
                    foreach($questions as $key=>$question){
                ?>
                <tr>
                    <td><?=$question['question']?></td>
                    <td>
                        <?=$_POST['answer'][$key]?>
                        <input type="hidden" name="answer[]" value="<?=$_POST['answer'][$key]?>">
                    </td>
                    <td><?=$question['max_point']?></td>
                    <td><input size="5" type="number" name="grade[]"></td>
                </tr>
                <?php
                    }
                ?>
            </table>
            <button class="send">Result</button>
        </form>
    </div>
</body>
</html>