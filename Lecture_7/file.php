<?php
    define("MB", 1024*1024);

    // echo MB;

    if(isset($_FILES['failis_saxeli'])){
        // echo "<pre>";
        // print_r($_FILES['failis_saxeli']);
        // echo "</pre>";
        if($_FILES['failis_saxeli']['size'] > MB){
            die("ERRORR!!!");
        }


        $patch = "storage/".$_FILES['failis_saxeli']['name'];
        
        if(!file_exists($patch)){
            move_uploaded_file($_FILES['failis_saxeli']['tmp_name'], $patch);
        }else{
            // echo "Error!!!";
            // echo time();
            $patch = "storage/".time().$_FILES['failis_saxeli']['name'];
            move_uploaded_file($_FILES['failis_saxeli']['tmp_name'], $patch);     
        }
    }


    if(isset($_POST['text'])){
       $file = fopen("files/data.txt", "a");
       fwrite($file, $_POST['text']); 
    }

    // print_r($_GET);

    $file_content = "";

    if(isset($_GET['act']) && $_GET['act']=="read"){
        $file = fopen("files/data.txt", "r");
        // $file_content .= fread($file, filesize("files/data.txt"));
        
        while(!feof($file)){
            $file_content .= fgets($file);
            $file_content .= "<hr>";
        }
        // $file_content .= fgets($file);
        // $file_content .= "<hr>";
        // $file_content .= fgets($file);
        fclose($file);
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lecture 7</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <main>
        <ul>
            <li>
                <a href="file.php">Home</a>
            </li>
            <li>
                <a href="?act=read">Read File</a>
            </li>
        </ul>
        <form method="post" enctype="multipart/form-data">
            <input type="file" name="failis_saxeli">
            <br>
            <br>
            <button>Upload File</button>
        </form>
        <div>
            <ol>
            <?php
                for($i=2; $i<count(scandir("storage")); $i++){
                   echo "<li>".scandir("storage")[$i]."</li>"; 
                }
            ?>
            </ol>
        </div>
        <br><br>
        <form method="post">
            <textarea name="text" cols="30" rows="10"></textarea>
            <br><br>
            <button>Write to File</button>
        </form>
        <div class="result">
            <?=$file_content?>
        </div>
    </main>
</body>
</html>